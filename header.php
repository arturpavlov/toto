<?php if (!defined('ABSPATH')) exit; ?>

<?php get_template_part('template-parts/head'); ?>

<header class="site-header">
    <!-- Header content goes here -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 d-flex">
                <div class="menu-btn d-lg-none js-menu">
                    <span class="toggler"></span>
                </div>
                <nav class="site-nav">
                    <?php pdg_language_switcher(); ?>
                    <?php pdg_nav('header', 'nav-items align-items-center'); ?>
                </nav>
                <?php if ($contact_us_btn = get_field('contact_us_btn', 'option')) : ?>
                    <a href="<?php echo $contact_us_btn['link'] ?>" class="contact-btn toto-btn"><?php echo $contact_us_btn['text'] ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
<?php $background_image = get_field('background_image');?>
<div class="site-content <?php if (!is_front_page() && !is_404()) echo "not-front-page"; ?>"style="<?php if ($background_image && !is_front_page()) echo "position:relative"; ?>
">