<?php get_header(); ?>
<section class="single-product">
    <?php if ($background_image = get_field('single_product_image', 'option')) : ?>
        <div class="background-texture" style="background-image: url(<?php echo pdg_get_image_src($background_image); ?>);"></div>
    <?php endif ?>
    <div class="container">
        <?php if ($return_to_products_link = get_field('return_to_products_link', 'option')) : ?>
            <a href="<?php echo $return_to_products_link ?>" class="return-back-btn"><span class="ic ic--arrow d-inline-block"></span><?php _e('Uz pārējiem produktiem', 'toto'); ?></a>
        <?php endif; ?>
        <div class="row">
            <?php
            $content_text = get_field('content_text');
            $content_table = get_field('content_table');
            $is_other_products = get_field('is_other_products')
            ?>
            <div class="col-md-6 image-block <?php if ($is_other_products) echo 'other-product-block'; ?>">
                <h2 class="product-name d-md-none product-name-mobile uppercase"></h2>
                <?php if ($content_text && !$is_other_products) : ?>
                    <div class="content d-md-none">
                        <?php echo $content_text ?>
                    </div>
                <?php endif ?>
                <div class="layer">
                    <?php
                    $size_array =  ($is_other_products) ? array(576, 839) : array(576, 670);

                    $image = get_field('image');
                    pdg_img($image, $size_array, array(
                        'class' => array('w-100'),
                        'fly' => true,
                        'crop' => true
                    ));
                    ?>
                </div>
            </div>
            <div class="col-md-6 offset-lg-1 col-lg-5 <?php if ($is_other_products) echo 'other-product-block d-flex flex-column'; ?>">
                <h2 class="product-name product-name-regular uppercase d-none d-md-block">
                    <?php echo get_the_title() ?>
                </h2>
                <?php if ($is_other_products) : ?>
                    <?php if (have_rows('other_products')) : ?>
                        <div class="row other-products">
                            <?php while (have_rows('other_products')) : the_row();
                                $image = get_sub_field('image');
                                $name = get_sub_field('name');
                            ?>
                                <div class="col-4">
                                    <div class="layer">
                                        <?php
                                        pdg_img($image, array(145, 225), array(
                                            'class' => array('w-100'),
                                            'fly' => true,
                                            'crop' => true
                                        ));
                                        ?>
                                    </div>
                                    <p><?php echo $name; ?></p>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                <?php endif ?>
                <?php if ($content_text) : ?>
                    <div class="content content-regular <?php echo ($is_other_products) ? 'other-product-content d-block order-2' : 'd-none d-md-block'; ?>

">
                        <?php echo $content_text ?>
                    </div>
                <?php endif ?>
                <?php if ($content_table) : ?>
                    <div class="content-table <?php if ($is_other_products) echo 'other-product-content'; ?>
">
                        <?php echo $content_table ?>
                    </div>
                <?php endif ?>

                <?php if ($contact_us_link = get_field('contact_us_btn', 'option')['link']) : ?>
                    <div class="ta--right <?php if ($is_other_products) echo 'order-3'; ?>">
                        <a href="<?php echo $contact_us_link ?>" class="read-more-btn"><?php _e('Sazināties ar mums', 'toto'); ?><span class="ic ic--arrow"></span></a>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>