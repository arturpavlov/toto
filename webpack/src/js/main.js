$(document).ready(function () {
  var $w = $(window);
  var $d = $(document);
  var $b = $("body");

  var toggle = false;
  var $menu = $(".js-menu");
  var $toggler = $(".toggler");
  var $siteNav = $(".site-nav");
  /*
   *  RESPONSIVE MENU BTN CLICK TO OPEN MENU
   */

  $d.mouseup((e) => {
    // if the target of the click isn't the container...
    if ($w.width() < 1100) {
      if (
        !$menu.is(e.target) &&
        $menu.has(e.target).length === 0 &&
        !$siteNav.is(e.target) &&
        $siteNav.has(e.target).length === 0
      ) {
        $toggler.removeClass("open");
        $siteNav.removeClass("open");
      }
    }
  });

  $menu.on("click", () => {
    $toggler.toggleClass("open");
    $siteNav.toggleClass("open");
  });

  function headerScrollChangeClass(headerEl) {
    if ($w.scrollTop() > 70) {
      headerEl.removeClass("site-header__top");
    } else {
      headerEl.addClass("site-header__top");
    }
  }

  /**
   * Front page navbar change class on scroll
   */
  var $siteHeader = $(".site-header");
  $w.bind("scroll", function () {
    headerScrollChangeClass($siteHeader);
  });

  headerScrollChangeClass($siteHeader);
});