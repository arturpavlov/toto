<?php get_header(); ?>

<?php if (!get_field('hide_page_title')) : ?>
    <div class="container">
        <h1 class="page-title"><?php the_title(); ?></h1>
    </div>
<?php endif; ?>

<?php if ($background_image = get_field('background_image')) :?>
    <img src="<?php echo pdg_get_image_src($background_image); ?>" class="background-texture <?php echo (is_front_page()) ? "d-none d-lg-block background-texture-home" : "background-texture-page"; ?>"/>
    <?php if(is_front_page()) : ?>
        <img src="<?php echo pdg_get_image_src($background_image, array( 760, 725 ), true, false); ?>" class="background-texture background-texture-home d-lg-none"/>
    <?php endif ?>
<?php endif?>
<div class="editor">
    <?php the_content(); ?>
</div>

<?php get_footer(); ?>