<?php if (!defined('ABSPATH')) exit; ?>

</div>

<footer class="site-footer">
    <!-- Footer content goes here -->
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="logo">
                        <?php if ($logo = get_field('logo', 'option')) : ?>
                            <?php
                            pdg_img($logo, 'full', array(
                                'class' => array('w-100'),
                                'fly' => false,
                                'crop' => false,
                                'svg_mode' => 2
                            ));
                            ?>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="col-lg-8">
                    <div class="row footer-info">
                        <div class="col-6 col-md-4 footer-nav-block">
                            <h3><?php _e('INFORMĀCIJA', 'toto'); ?></h3>
                            <?php pdg_nav('footer', 'footer-nav'); ?>
                        </div>
                        <?php if ($contacts = get_field('contacts', 'option')) : ?>
                            <div class="col-6 col-md-4 address-block">
                                <h3><?php _e('SAZIŅAI', 'toto'); ?></h3>
                                <?php if ($contacts['address']) : ?>
                                    <p class="address"><?php echo $contacts['address'] ?></p>
                                <?php endif ?>

                                <?php if ($contacts['phone_number']) : ?>
                                    <p>tel.: <a href="tel:<?php echo $contacts['phone_number'] ?>"><?php echo $contacts['phone_number'] ?></a></p>
                                <?php endif ?>
                                <?php if ($contacts['email']) : ?>
                                    <p>e-pasts: <a href="mail:<?php echo $contacts['email'] ?>"><?php echo $contacts['email'] ?></a></p>
                                <?php endif ?>
                                <?php get_template_part('template-parts/social-nav'); ?>

                            </div>
                        <?php endif ?>
                        <?php if ($sertification_image = get_field('sertification_image', 'option')) : ?>
                            <div class="offset-md-1 col-md-3 sertification-block">
                                <h3><?php _e('SERTIFIKĀCIJA', 'toto'); ?></h3>
                                <div class="sertification-block__image d-flex align-items-center justify-content-center">
                                    <span class="ic ic--acs eye"></span>
                                    <a href="<?php echo pdg_get_image_src($sertification_image); ?>" data-fancybox class="sertification">
                                        <?php
                                        pdg_img($sertification_image, array(164, 127), array(
                                            'class' => array('w-100'),
                                            'fly' => true,
                                            'crop' => true
                                        ));
                                        ?>
                                    </a>
                                </div>
                            </div>
                        <?php endif ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-12 flex align-items-center">
                    <?php get_template_part('template-parts/copyright'); ?>
                    <?php $privacyPolicyLink = get_privacy_policy_url(); ?>

                    <div class="additional-block d-flex justify-content-between align-items-center">
                        <?php if (!empty($privacyPolicyLink)) : ?>
                            <a class="privacy-policy-link" href="<?php echo $privacyPolicyLink ?>"><?php _e('Privātuma politika', 'toto'); ?></a>
                        <?php endif ?>
                        <?php get_template_part('template-parts/developer'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php get_template_part('template-parts/foot'); ?>
<script>
    console.log('<?php echo wp_login_url(get_permalink()) ?>');
</script>