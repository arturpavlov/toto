<?php if (!defined('ABSPATH')) exit; ?>

<?php get_header(); ?>
<section class="not-found">
    <?php if ($background_image = get_field('single_product_image', 'option')) : ?>
        <div class="background-texture" style="background-image: url(<?php echo pdg_get_image_src($background_image); ?>);"></div>
    <?php endif ?>
    <div class="c-404 container text-center">
        <h1 class="c-404__title">404</h1>

        <h2 class="c-404__sub-title h3"><?php _e('Lapa netika atrasta', 'pandago'); ?></h2>

        <p class="c-404__message"><?php _e('Radusies kāda tehniska kļūda, vai arī šī lapa vairs nav pieejama.', 'pandago'); ?></p>

        <div class="c-404__btn-wrap">
            <a class="toto-btn" href="<?php echo esc_url(home_url()); ?>"><?php _e('Uz sākumlapu', 'pandago'); ?></span></a>
        </div>
    </div>
</section>


<?php get_footer(); ?>