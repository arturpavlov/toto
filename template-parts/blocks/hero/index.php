<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <?php if ($logo = get_field('logo')) : ?>
                    <?php
                    pdg_img($logo, array( 163, 95 ), array(
                        'class' => array('logo'),
                        'fly' => false,
                        'crop' => false,
                        'dimensions' => true,
                        'svg_mode' => 2
                    ));
                    ?>
                <?php endif; ?>
                <?php if ($title = get_field('title')) : ?>
                    <h1><?php echo $title ?></h1>
                <?php endif ?>
                <?php if ($description = get_field('description')) : ?>
                    <p><?php echo $description ?></p>
                <?php endif ?>

            </div>
        </div>
    </div>
</section>