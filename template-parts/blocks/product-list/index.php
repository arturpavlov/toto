<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="product-list">
    <div class="container">
        <?php
        $number = get_field('number');
        $title = get_field('title');
        if ($number || $title) {
            $args = array('title' => $title, 'number' => $number);
            get_template_part('template-parts/block-title', null, $args);
        } ?>
        <div class="row">
            <?php

            $args = array(
                'post_type' => 'product',
                'posts_per_page' => -1
            );
            $query = new WP_Query($args);
            if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-lg-3 col-md-4">
                        <?php
                        $image = get_field('image', get_the_ID());
                        $src   = pdg_get_image_src($image, array(329, 590), true, false);
                        ?>
                        <a href="<?php the_permalink(); ?>" class="w-100 d-block layer text-decor-none">

                            <div class="product-image" style="background-image: url(<?php echo $src ?>);"></div>

                            <h3 class="uppercase d-flex align-items-center">
                                <?php echo get_the_title() ?><span class="ic ic--arrow"></span>
                            </h3>
                        </a>
                    </div>
            <?php endwhile;
                wp_reset_postdata();
            endif;

            ?>

        </div>
    </div>
</section>