<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<?php if (have_rows('statistics_items')) : ?>
    <section class="statictics">
        <div class="container">
            <?php
            $number = get_field('number');
            $title = get_field('title');
            if ($number || $title) {
                $args = array('title' => $title, 'number' => $number);
                get_template_part('template-parts/block-title', null, $args);
            } ?>
            <div class="row">
                <?php while (have_rows('statistics_items')) : the_row();
                    $item_amount = get_sub_field('item_amount');
                    $item_title = get_sub_field('item_title');
                    $item_texture = get_sub_field('item_texture');
                ?>
                    <div class="col-6 col-lg-3 text-center">
                        <?php if ($item_texture) : ?>
                            <div class="texture-wrapper d-flex align-items-center">
                                <?php
                                pdg_img($item_texture, 'full', array(
                                    'class' => array('texture'),
                                    'fly' => false,
                                    'crop' => false,
                                    'svg_mode' => 2
                                ));
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="item-amount js-not-visible" counter-lim="<?php echo $item_amount ?>"></div>
                        <div class="item-title"><?php echo $item_title ?></div>
                    </div>

                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>