$(document).ready(function () {
  // Check if an element is on screen
  $.fn.isOnScreen = function () {
    var win = $(window);

    var viewport = {
      top: win.scrollTop(),
      left: win.scrollLeft(),
    };

    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return !(
      viewport.right < bounds.left ||
      viewport.left > bounds.right ||
      viewport.bottom < bounds.top ||
      viewport.top > bounds.bottom
    );
  };


  $(window).scroll(function () {
    $(".item-amount").each(function () {
      // Check if counter is visible
      if ($(this).isOnScreen()) {
        // Start counter
        startCounter($(this));
      }
    });
  });

  function startCounter(counterElement) {
    if (counterElement.hasClass("js-not-visible")) {
      // Run counter animation
      counterElement.prop("Counter", 0).animate(
        {
          Counter: counterElement.attr("counter-lim"),
        },
        {
          duration: 500,
          easing: "swing",
          step: function (now) {
            counterElement.text(Math.ceil(now).toLocaleString());
          },
        }
      );
      counterElement.removeClass("js-not-visible");
    }
  }

  // On page load check if counter is visible
  $(".item-amount").each(function () {
    // Check if element is visible on page load
    if ($(this).isOnScreen() === true) {
      // If visible, start counter
      startCounter($(this));
    }
  });
});
