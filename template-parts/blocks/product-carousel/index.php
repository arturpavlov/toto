<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1
);

$query = new WP_Query($args);
if ($query->have_posts()) : ?>
    <section class="product-carousel">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="slick-product-carousel">
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <div class="product-item">
                                <?php
                                $image = get_field('image', get_the_ID());
                                $src   = pdg_get_image_src($image, array(316, 640), true, true);
                                ?>
                                <div class="product-image" style="background-image: url(<?php echo $src ?>);"></div>
                                <a href="<?php the_permalink(); ?>" class="layer w-100 d-block text-decor-none">
                                    <h3 class="uppercase">
                                        <?php echo get_the_title() ?>
                                    </h3>
                                    <span class="read-more-text"><?php _e('Skatīt vairāk', 'toto'); ?></span>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="slick-custom-arrows d-flex justify-content-end align-items-center">
                        <span class="ic ic--arrow prev"></span>
                        <span class="ic ic--arrow next"></span>
                        <div class="pagingInfo"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php wp_reset_postdata();
endif; ?>