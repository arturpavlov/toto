$(document).ready(function () {
  var $status = $(".pagingInfo");
  var $slickElement = $(".slick-product-carousel");

  function isNumberLessThanTen(number) {
    return number < 10;
  }

  if ($(".slick-product-carousel").length) {
    $slickElement.on(
      "init reInit afterChange",
      function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        if (!slick.$dots || !slick.options.slidesToScroll) {
          return;
        }
        var i =
          (currentSlide
            ? Math.ceil(currentSlide / slick.options.slidesToScroll)
            : 0) + 1;
        console.log(isNumberLessThanTen(i));
        var before_current_page_number = isNumberLessThanTen(i) ? 0 : "";
        var before_total_pages_number = isNumberLessThanTen(
          slick.$dots[0].children.length
        )
          ? 0
          : "";

        var current_page =
          '<span class="current-page">' +
          before_current_page_number +
          +i +
          "</span>";

        var total_pages =
          '<span class="total-pages">' +
          before_total_pages_number +
          slick.$dots[0].children.length +
          "</span>";
        $status.html(
          current_page + '<span class="separator">/</span>' + total_pages
        );
      }
    );
    $(".slick-product-carousel").slick({
      lazyLoad: 'ondemand',
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      nextArrow: $(".next"),
      prevArrow: $(".prev"),
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true,
            variableWidth: true,
          },
        },
        {
          breakpoint: 767.98,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            variableWidth: true,
          },
        },
      ],
    });
  }
});
