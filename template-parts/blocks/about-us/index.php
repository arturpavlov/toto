<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="about-us">
    <div class="container">
        <div class="row align-items-center">
            <?php if ($description = get_field('description')) : ?>
                <div class="col-lg-6 about-us-block">
                    <?php
                    $number = get_field('number');
                    $title = get_field('title');
                    if ($number || $title) {
                        $args = array('title' => $title, 'number' => $number);
                        get_template_part('template-parts/block-title', null, $args);
                    } ?>
                    <?php echo $description ?>
                </div>
            <?php endif ?>
            <?php if ($image = get_field('image')) : ?>
                <div class="col-lg-6 image-col d-flex justify-content-end">
                    <div class="layer">
                        <?php
                        pdg_img($image, array(476, 610), array(
                            'class' => array('d-block d-md-none d-lg-block'),
                            'fly' => true,
                            'crop' => true
                        ));
                        ?>
                        <?php
                        pdg_img($image, array(678, 318), array(
                            'class' => array('d-none d-md-block d-lg-none'),
                            'fly' => true,
                            'crop' => true
                        ));
                        ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>