<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="services">
    <div class="container">
        <div class="row">
            <?php if ($description = get_field('description')) : ?>
                <div class="col-lg-5 description-block">
                    <?php
                    $number = get_field('number');
                    $title = get_field('title');

                    if ($number || $title) {
                        $args = array('title' => $title, 'number' => $number);
                        get_template_part('template-parts/block-title', null, $args);
                    } ?>
                    <div class="description">
                        <?php echo $description ?>
                    </div>
                    <?php if ($read_more_link = get_field('read_more_link')) : ?>
                        <a href="<?php echo $read_more_link ?>" class="read-more-btn d-flex align-items-center"><?php _e('Lasīt vairāk', 'toto'); ?><span class="ic ic--arrow"></span></a>
                    <?php endif ?>
                </div>
            <?php endif; ?>
            <?php
            $list_title = get_field('list_title');
            $list_items = get_field('list_items')
            ?>
            <?php if ($list_title || $list_items) : ?>
                <div class="col-lg-3 col-md-5 list-block">
                    <?php if ($list_title) : ?>
                        <p class="list-title"><?php echo $list_title ?></p>
                    <?php endif; ?>
                    <?php if (have_rows('list_items')) : ?>
                        <ul>
                            <?php while (have_rows('list_items')) : the_row();
                                $bullet_icon = get_sub_field('bullet_icon');
                                $item_text = get_sub_field('item_text');
                            ?>
                                <li><i class="ic ic--<?php echo $bullet_icon; ?>"></i>
                                    <p><?php echo $item_text; ?></p>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if ($image = get_field('image')) : ?>
                <div class="col-lg-4 col-md-7">
                    <div class="layer">
                        <?php
                        pdg_img($image, array(377, 369), array(
                            'class' => array('w-100'),
                            'fly' => true,
                            'crop' => true
                        ));
                        ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>