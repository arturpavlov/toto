<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="we-buy">
    <div class="container">

        <?php
        $number = get_field('number');
        $title = get_field('title');
        if ($number || $title) {
            $args = array('title' => $title, 'number' => $number);
            get_template_part('template-parts/block-title', null, $args);
        } ?>
        <div class="row">
            <?php if ($description = get_field('description')) : ?>
                <div class="col-lg-6">
                    <div class="description">
                        <?php echo $description ?>
                    </div>
                </div>
            <?php endif ?>
            <div class="col-lg-6">
                <?php if ($before_content_text = get_field('before_content_text')) : ?>
                    <p class="before-content ta--right"><?php echo $before_content_text ?></p>
                <?php endif ?>
                <?php if ($content = get_field('content')) : ?>
                    <div class="content">
                        <?php echo $content ?>
                    </div>
                <?php endif ?>
                <?php if ($contact_us_link = get_field('contact_us_btn', 'option')['link']) : ?>

                    <a href="<?php echo $contact_us_link ?>" class="read-more-btn d-flex align-items-center justify-content-end"><?php _e('Sazināties ar mums', 'toto'); ?><span class="ic ic--arrow"></span></a>

                <?php endif ?>
            </div>
        </div>
    </div>
</section>