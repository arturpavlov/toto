<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>

<?php if (have_rows('specialists')) : ?>
    <section class="specialists">
        <div class="container">
            <div class="row">
                <?php while (have_rows('specialists')) : the_row();
                    $name = get_sub_field('name');
                    $position = get_sub_field('position');
                    $contacts = get_sub_field('contacts');
                    $texture = get_sub_field('texture');
                ?>
                    <div class="col-lg-3 col-md-6 text-center d-flex flex-column justify-content-center">
                        <?php if ($texture) : ?>
                            <div class="texture-wrapper d-flex align-items-center">
                                <?php
                                pdg_img($texture, 'full', array(
                                    'class' => array('texture'),
                                    'fly' => false,
                                    'crop' => false,
                                    'svg_mode' => 2
                                ));
                                ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($name) : ?>
                            <h3 class="name uppercase"><?php echo $name ?></h3>
                        <?php endif ?>

                        <?php if ($position) : ?>
                            <p class="position"><?php echo $position ?></p>
                        <?php endif ?>

                        <?php if ($contacts) : ?>
                            <div class="contacts">
                                <?php if ($contacts['phone_number']) : ?>
                                    <div class="phone">
                                        <p>tel.: <a href="tel:<?php echo $contacts['phone_number'] ?>"><?php echo $contacts['phone_number'] ?></a></p>
                                    </div>
                                <?php endif ?>
                                <?php if ($contacts['phone_number']) : ?>
                                    <div class="email">
                                        <p>e-pasts: <a href="mail:<?php echo $contacts['email'] ?>"><?php echo $contacts['email'] ?></a></p>
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php endif ?>

                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>