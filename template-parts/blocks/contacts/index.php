<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="contacts">
    <div class="container">
        <div class="row">
            <div class="contact-col">
                <div class="row">
                    <?php if ($requisites = get_field('requisites')) : ?>
                        <div class="col-md-6">
                            <div class="requisites-block">
                                <h2 class="uppercase"><?php _e('REKVIZĪTI', 'toto'); ?></h2>
                                <?php echo $requisites; ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php $global_contacts = get_field('contacts', 'option') ?>
                    <?php if ($contacts = get_field('contacts')) : ?>
                        <div class="col-md-6">
                            <div class="contact-block">
                                <h2 class="uppercase"><?php _e('KONTAKTI', 'toto'); ?></h2>
                                <?php if ($global_contacts) : ?>
                                    <div class="global-contacts">
                                        <?php if ($show_phone_number = get_field('show_phone_number') && $global_contacts['phone_number']) : ?>
                                            <p>tel.: <a href="tel:<?php echo $global_contacts['phone_number'] ?>"><?php echo $global_contacts['phone_number'] ?></a></p>
                                        <?php endif ?>

                                        <?php if ($show_email = get_field('show_email') && $global_contacts['email']) : ?>
                                            <p>e-pasts: <a href="mail:<?php echo $global_contacts['email'] ?>"><?php echo $global_contacts['email'] ?></a></p>
                                        <?php endif ?>
                                    </div>
                                <?php endif ?>

                                <div class="another-contacts">
                                    <?php echo $contacts; ?>
                                </div>
                                <?php if ($google_location = get_field('google_location')) : ?>
                                    <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php echo get_bloginfo('name') . str_replace("<br />", "", $global_contacts['address']) ?>" class="google-location text-decor-none"><i class="ic ic--location-icon"></i><span><?php _e('Google lokācija', 'toto'); ?></span></a>
                                <?php endif ?>
                            </div>

                        </div>
                    <?php endif ?>
                    <?php if ($contact_form_shortcode = get_field('contact_form_shortcode')) : ?>
                        <div class="col-12 contact-us-block" id="contact-us">
                            <h2 class="uppercase"><?php _e('Sazinies ar mums', 'toto'); ?></h2>
                            <div data-pdg-cf7>
                                <?php echo do_shortcode($contact_form_shortcode) ?>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <?php if ($map_frame = get_field('iframe', 'options')) : ?>
                <div class="map-col">
                    <div class="map w-100 layer">
                        <iframe src="<?php echo $map_frame ?>&z=10&layer=0"></iframe>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>