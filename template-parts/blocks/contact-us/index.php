<?php if (!defined('ABSPATH')) exit;
if (is_admin()) return; ?>
<section class="contact-us">
    <div class="container">
        <div class="row">
            <div id="contact-us" class="col-lg-6 contact-form-block">
                <?php
                $number = get_field('number');
                $title = get_field('title');
                if ($number || $title) {
                    $args = array('title' => $title, 'number' => $number);
                    get_template_part('template-parts/block-title', null, $args);
                }
                $contact_form_shortcode = get_field('contact_form_shortcode'); ?>
                <div data-pdg-cf7>
                    <?php echo do_shortcode($contact_form_shortcode); ?>
                </div>

            </div>
            <?php if ($map_frame = get_field('iframe', 'options')) : ?>
                <div class="col-lg-6 map-block">
                    <div class="map layer" id="map">
                        <iframe src="<?php echo $map_frame ?>&z=10&layer=0"></iframe>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>