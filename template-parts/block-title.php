<div class="title-block">
    <?php if (isset($args['number'])) : ?>
        <div class="number"><?php echo $args['number'] ?></div>
    <?php endif ?>
    <?php if (isset($args['title'])) : ?>
        <h2 class="uppercase"><?php echo $args['title'] ?></h2>
    <?php endif ?>
</div>