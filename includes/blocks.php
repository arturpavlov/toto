<?php if (!defined('ABSPATH')) exit;

function pdgc_acf_blocks()
{
    pdg_add_acf_block('Hero', true);
    pdg_add_acf_block('Our services', true);
    pdg_add_acf_block('Statistics', true, true);
    pdg_add_acf_block('Contact us', true, false, function () {
        wp_enqueue_style('form-inputs', PDGC_ASSETS . '/vendor/theme/form-inputs/form-inputs.css', array(), PDGC_VER);
        wp_enqueue_script('pdg-google-maps');
    });
    pdg_add_acf_block('We buy', true);
    pdg_add_acf_block('About us', true);
    pdg_add_acf_block('Specialists', true);
    pdg_add_acf_block('Product carousel', true, true, function () {
        wp_enqueue_style('pdg-slick');
        wp_enqueue_script('pdg-slick');
    });
    pdg_add_acf_block('Product list', true);
    pdg_add_acf_block('Contacts', true, false, function () {
        wp_enqueue_style('form-inputs', PDGC_ASSETS . '/vendor/theme/form-inputs/form-inputs.css', array(), PDGC_VER);
        wp_enqueue_script('pdg-google-maps');
    });
}
add_action('acf/init', 'pdgc_acf_blocks');
