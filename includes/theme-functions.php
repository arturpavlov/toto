<?php if (!defined('ABSPATH')) exit;

add_action('wp_print_scripts', 'deregister_cf7_script', 100);
function deregister_cf7_script()
{
    wp_deregister_script('google-recaptcha');
}
