<?php if (!defined('ABSPATH')) exit;

function pdgc_add_assets()
{

    // Main theme assets.
    wp_enqueue_style('pdgc-main', PDGC_ASSETS . '/css/main.css', array(), PDGC_VER);
    wp_enqueue_script('pdgc-main', PDGC_ASSETS . '/main.js', array('jquery'), PDGC_VER, true);
    // Late loaded assets.
    wp_enqueue_style('pdgc-late', PDGC_ASSETS . '/vendor/theme/late.css', array(), PDGC_VER);
    wp_enqueue_script('pdgc-late', PDGC_ASSETS . '/vendor/theme/late.js', array(), PDGC_VER, true);
    
    if (is_singular('product')) {
        wp_enqueue_style('single-product', PDGC_ASSETS . '/vendor/theme/product-page/style.css', array(), PDGC_VER);
        wp_enqueue_script('single-product', PDGC_ASSETS . '/vendor/theme/product-page/script.js', array(), PDGC_VER, true);
    }

    if (get_field('sertification_image', 'option')) {
        wp_enqueue_style('pdg-fancybox');
        wp_enqueue_script('pdg-fancybox');
    }

    if (is_privacy_policy()) {
        wp_enqueue_style('pdgc-privacy', PDGC_ASSETS . '/vendor/theme/privacy.css', array(), PDGC_VER);
    }

    if (is_404()) {
        wp_enqueue_style('additional-404', PDGC_ASSETS . '/vendor/theme/404.css', array(), PDGC_VER);
    }
    wp_enqueue_style('custom-cookies', PDGC_ASSETS . '/vendor/theme/cookies/cookies.css', array(), PDGC_VER);
    wp_enqueue_script('custom-cookies', PDGC_ASSETS . '/vendor/theme/cookies/cookies.js', array(), PDGC_VER, true);
    
    wp_enqueue_script('async-recaptcha', PDGC_ASSETS . '/vendor/theme/async_recaptcha.js', array(), PDGC_VER, true);
}
add_action('wp_enqueue_scripts', 'pdgc_add_assets', 20);
